Title: Frequently Asked Questions
Slug: faq
Modified: April 26, 2018
Summary: Information about random aspects of the site

[TOC]

## Why is the site named "Tildes"?

tl;dr: On Tildes, the tilde symbol (`~`) is used to mark sections of the site: ~music, ~games, ~tv, and so on.

The path that led to "Tildes" was a bit strange. Originally, I wanted to have a name related to the word "spectrum". I think that's a great term for describing an online community platform&mdash;a wide range of variance inside a whole. That's why the non-profit behind the site is named "Spectria".

As part of thinking about other topics related to a spectrum, I ended up on waves and waveforms, which led to realizing that the tilde symbol (~) looks like a tiny wave. For multiple reasons, I started really liking the idea of using a tilde as the "marker" for a community on the site (for example, the music community would be "~music").

First, tilde is one of the only "unreserved" characters that can be used in web addresses (URIs). From [the RFC related to URIs](http://www.ietf.org/rfc/rfc3986.txt):

> Characters that are allowed in a URI but do not have a reserved purpose are called unreserved.  These include uppercase and lowercase letters, decimal digits, hyphen, period, underscore, and tilde.

That means that a tilde can always be used in a web address without needing to be escaped. This isn't true for many other symbols&mdash;for example, some sites try to put an `@` character in their addresses (usually related to usernames), but since that's not an unreserved character, it will often get converted to `%40`, which looks much uglier. A tilde should always be kept as a tilde.

In addition, the `~` symbol also has an association of "home" to many technical people. If you're using [the Bash shell](https://en.wikipedia.org/wiki/Bash_\(Unix_shell\)) (or various others), a tilde can often be used to refer to the user's home location. For example, the command `cd ~` changes directory to your home dir. A command like `cd ~deimos` will go to the home dir of the user `deimos`, and so on. I like the idea of each community being thought of as "the home for &lt;topic&gt;".

It's also a bit of a throwback to common addresses on the early web, where users would host their website on a shared system under their username. For example, when I was in university, the address of my website hosted on the Computer Science department's server was something like http://pages.cpsc.ucalgary.ca/~cbirch. [Paul Ford caused a fun resurgence of this a few years ago when he started Tilde.Club](https://medium.com/message/tilde-club-i-had-a-couple-drinks-and-woke-up-with-1-000-nerds-a8904f0a2ebf).

So in the end, a bunch of technical, historical, and associational reasons convinced me that I definitely wanted to use the tilde symbol. From there, it didn't take much until the symbol of the site turned into the actual name.

## Why is Tildes a non-profit and not a charity?

Canada only grants charity status to [organizations with certain purposes](https://www.canada.ca/en/revenue-agency/services/charities-giving/giving-charity-information-donors/about-registered-charities/what-difference-between-a-registered-charity-a-non-profit-organization.html). Generally, the organization has to be devoted to relieving poverty, advancing religion or education, or the benefit of the (local, real-life) community. These are quite restricted definitions&mdash;note that [Wikimedia Canada](https://ca.wikimedia.org/wiki/About_us) (the Canadian branch of the organization behind Wikipedia) is also a non-profit and not a charity. If even building Wikipedia doesn't seem to qualify as "advancing education", I don't think there's any chance that Tildes will.

## Why isn't Tildes decentralized/distributed/federated?

Decentralized communities are interesting and have a lot of potential, but that model also introduces its own problems and difficulties. Tildes is already attempting to do quite a few things differently to improve the quality of online communities, and I'm more interested in focusing on those goals without introducing the additional complexity of decentralization.

However, since Tildes will be open-source, someone else could certainly use it as a base for their own decentralized version.

## Does Tildes allow non-English communities?

Not for now. Multiple of the site's goals will be difficult or impossible to work towards without being able to understand what's going on in a community, so for now they need to be primarily in English. This may change someday in the future, and if it does, the [hierarchical groups](https://docs.tildes.net/mechanics) could work very well for giving other languages their own set of groups.

## What's the color scheme used on Tildes?

Tildes uses [the Solarized color scheme by Ethan Schoonover](http://ethanschoonover.com/solarized). It's always been one of my personal favorite schemes, and it has some interesting aspects such as flipping very easily between dark and light modes.

## What if you don't get enough donations to run the site full-time?

One of the best parts about avoiding venture capital and other forms of investment is that there's no pressure. Tildes doesn't have to reach certain thresholds of traffic or revenue to prevent shutting down. The worst case is just that I end up running Tildes as a side project, and hope that it eventually grows to a point where it's sustainable to work on full-time.
