Title: Mechanics
Slug: mechanics
Modified: May 4, 2018
Summary: Description of the basic mechanics on Tildes, and some reasoning behind them

[TOC]

This page is an (incomplete) description of the mechanics on Tildes. None of these mechanics are particularly original&mdash;Tildes is intended to be a refinement of community sites, not an entirely new type.

## Groups

The different subject-oriented sections on Tildes are called "groups". They have a tilde symbol in front of their name&mdash;for example, the group related to music is "~music".

It's not being used yet in the alpha, but groups will eventually be organized *hierarchically*. That is, groups can have sub-groups, and be organized into a "tree". In the future, the ~music group could have sub-groups such as ~music.metal, and it could have its own sub-groups like ~music.metal.instrumental. This concept should be recognizable to anyone familiar with [Usenet](https://en.wikipedia.org/wiki/Usenet).

Groups are not "owned" by users, and (at least for now) can not be created by users. This may change in the future, but the lack of user-created groups initially will make it simpler to keep the hierarchy organized, as well as concentrate activity in fewer groups while the site is still small.

A small set of active groups is far better than a large set of inactive ones, and the hierarchy will allow different subjects to easily split into more-specific groups as activity increases.

## Topics

The posts made to groups are called "topics". There are two types of topics: link topics, which point to a specific url; and text topics, where the text is posted on Tildes itself by the author.

Users can vote on other users' topics, but there is only a positive vote, not a negative one ([see below for more about this](#lack-of-downvoting)).

### Topic tags

Topics can be tagged in order to categorize them. Currently, tagging can only be done by the same user that posted the topic, but other users will eventually be able to add/edit/remove tags as well.

A topic can have any number of tags, and the valid characters in a tag are letters, numbers, and spaces.

It will most likely not be used very frequently, but tags also support a hierarchy. For example, a topic tagged `nsfw.gore` is using the hierarchy for a "sub-tag" of the `nsfw` tag. This has some interesting possibilities&mdash;someone filtering out all `nsfw` posts would also not see anything tagged `nsfw.gore`, but someone else could choose to filter *only* `nsfw.gore` and still see posts with other tags like `nsfw.nudity` or plain `nsfw`.

It's worth mentioning that the hierarchy and naming restrictions of topic tags match up exactly with how groups work. This is deliberate&mdash;there's a lot of potential here with the parallels between sub-groups and tags.

## Comments

Tildes has nested comments, which form a tree structure. Similar to topics, other users' comments can be voted on, and there is no downvote ([see below for more about this](#lack-of-downvoting)).

### Comment tags

Comments can also be tagged, which is separate from voting (you can vote on a comment, or tag it, or do both). Tags help to categorize comments and can have other effects. Currently, there are 5 options for tagging comments:

* Joke - comments posted for humor purposes (actual jokes, puns, references, etc.)
* Noise - comments that don't add anything to the discussion ("lol", "I agree", responses to the headline like "finally!", etc.)
* Offtopic - comments talking about something unrelated to the actual topic
* Troll - comments posted for the purpose of getting a reaction
* Flame - comments attacking another user

Comment tags serve multiple purposes. Tildes has no downvoting, but some tags can effectively act as "downvote with a reason". Tags will also make it possible to support various methods of filtering comment threads, such as both "show this thread without jokes" and "show *only* jokes from this thread".

This concept is probably familiar to anyone who's used Slashdot. Those people have probably also noticed that the positive options from Slashdot are missing, specifically "interesting" and "insightful". In my opinion, tags like that don't add anything meaningful&mdash;I'd never open a comment thread and think, "I'd like to read the interesting comments, but not the insightful ones."

Overall, voting on a comment should mean something like "this is a good comment and I think other people should read it", while tagging a comment adds more information. With the combination of both, you can express things like "this is a good comment, even though it's off-topic", and "this is a joke, but it's a good one".

## Lack of downvoting

As mentioned above, Tildes does not have negative votes for either topics or comments. The reason for this is that I believe we can implement different mechanics that replace the "proper" use of downvotes without also enabling all the misuses of them.

The ideal usage of a downvote is a generic way to express "this doesn't contribute", but in practice they tend to be used more as "I disagree" or "I don't like this". High-quality posts will often get downvoted because other users disagree with the opinion, and in taste-based communities (such as ones related to music), entire categories of valid posts might be unviable because they'll just be downvoted by users with different taste.

On Tildes, I want to find ways to accomplish those valuable uses through other mechanics. For example, the [comment tags](#comment-tags) described above can be used to communicate *why* you don't think a comment contributes. [Topic tags](#topic-tags) will allow users to simply filter out certain types of posts that they're not interested in, instead of downvoting them and hurting them for other users that *do* want to see them.
