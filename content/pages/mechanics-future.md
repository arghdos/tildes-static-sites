Title: Mechanics (Future)
Slug: mechanics-future
Modified: May 11, 2018
Summary: Information about some future mechanics not present in the alpha

[TOC]

*This page describes general plans for future mechanics, which are not yet present in the alpha version of Tildes. The details of how they work will most likely evolve significantly as they are implemented and experimented with.*

*For information about current mechanics, see [the Mechanics page](https://docs.tildes.net/mechanics).*

## Trust/reputation system for moderation

One of the few constants of online communities throughout their whole existence has been that they tend to start out good, but have trouble maintaining their culture as they grow, their quality dives rapidly, and they die. It's been happening from the very beginning with examples like [CommuniTree (one of the first BBSes)](http://software.bbsdocumentary.com/APPLE/II/COMMUNITREE/vampires-excerpt.txt), [Usenet with the well-known "Eternal September"](https://en.wikipedia.org/wiki/Eternal_September), and it's continuing to happen today.

One of the most common ways that communities defend themselves is by appointing moderators&mdash;people entrusted with defining and enforcing the norms of behavior for the community. This is an effective system, but has its own weaknesses, including difficult decisions about which users should be made (and allowed to remain) moderators.

In my experience, it's always been the best approach to select new moderators from the people known as active, high-quality members of the community. My goal with the trust system on Tildes is to turn this process of discovering the best members and granting them more influence into a natural, automatic one.

It's worth noting that the process does not need to be *entirely* automatic. The trust system won't necessarily be a complete replacement for manually promoting users, and a combination of both systems may end up working best.

### Trust based on consistency and accountability

Trusting someone is a gradual process that comes from seeing how they behave over time. This can be reflected in the site's mechanics&mdash;for example, if a user consistently reports posts correctly for breaking the rules, eventually it should be safe to just trust that user's reports without preemptive review. Other users that aren't as consistent can be given less weight&mdash;perhaps it takes three reports from lower-trust users to trigger an action, but only one report from a very high-trust user.

This approach can be applied to other, individual mechanics as well. For example, a user could gain (or lose) access to particular abilities depending on whether they use them responsibly. If done carefully, this could even apply to voting&mdash;just as you'd value the recommendation of a trusted friend more than one from a random stranger, we should be able to give more weight to the votes of users that consistently vote for high-quality posts.

### Restricted by group, with decay

Trust should be largely *group-specific*. That is, users should need to actively participate in a particular community to build up trust in it. Because [Tildes will have a hierarchy of groups](https://docs.tildes.net/mechanics#groups), there are some possibilities with having trust work inside the "branches"&mdash;for example, a user that's highly trusted in one music-related group could be given some inherent trust in *other* music-related ones, but not necessarily anything in groups related to, say, TV shows.

Another important factor will be having trust decay if the user stops participating in a community for a long period of time. Communities are always evolving, and if a user has been absent for months or years, it's very likely that they no longer have a solid understanding of the community's current norms. Perhaps users that previously had a high level of trust should be able to build it back up more quickly, but they shouldn't indefinitely retain it when they stop being involved.

Between these two factors, we should be able to ensure that communities end up being managed by members that actively contribute to them, not just people that want to be a moderator for its own sake.

### Increased punishment effectiveness

One of the core reasons that platforms have so many issues with abuse is that their punishments have little impact. Banned users are often able to immediately create a new account that has identical capabilities to their previous one. Trying to remove persistent malicious users can be an endless game of whack-a-mole where it requires more effort to punish abusers than it does for them to circumvent it.

By having users gradually build up trust in individual communities, "established" accounts can be far more capable than brand new ones, which adds some actual weight to punishments. If implemented well, this should cause little inconvenience for regular users, but make it far, far more difficult for malicious users to cause trouble.

### Concerns

To be clear, I recognize that this is a dangerous type of system to implement, with the distinct risk of creating "power users" that have far too much influence. However, all systems have similar risks&mdash;even if all users are equal, people can form groups or abuse multiple accounts to increase their influence. These types of issues are social and can only be solved with oversight, accountability, and a willingness to punish people that abuse the system, not technology alone.

Many aspects of this system will need careful observation and tweaking to ensure it works as desired. We don't want to end up incentivizing the wrong types of behavior by creating systems that, for example, give more influence to the *most popular* users instead of the *highest quality* ones. It won't be a simple process, but I believe a system like this will be able to make a huge difference in maintaining the quality of a community as it grows.

## Feature Discussions

This is an (incomplete) list of all the great discussions on [~tildes](https://tildes.net/~tildes) about Tildes features, changes and their implementation.
Not all of these will necessarily be implemented, but provide a reference point for the user

- [Community Moderators?](https://tildes.net/~tildes/6e/community_moderators) How do we moderate effectively, and fairly? Do we moderate at all? Everyone should read this monster as well as [the follow-up thread](https://tildes.net/~tildes/vf/the_future_of_moderation_on_tildes)
- [How do we handle communities that get too large?](https://tildes.net/~talk/vs/what_is_tildes_plan_for_communities_that_get_too_large) It's a doozy, the inverse of reddit.
- [Do we allow Fluff content?](https://tildes.net/~tildes/jz/daily_tildes_discussion_why_should_we_allow_or_not_allow_fluff_content) Just how do we select for quality instead of popularity, or even define quality, anyway?
- [What do we do about "Fake News?"](https://tildes.net/~tildes/jb/thoughts_on_addressing_the_filter_bubble_echo_chambers_fake_news_scalability_free_speech)
- [Can we create new ~groups?](https://tildes.net/~tildes/ly/ability_to_create_new_s_on_the_site) Will users be able to create them? Yes, eventually... but it's not that simple.
- [How do we stop bots from wrecking the place?](https://tildes.net/~tildes/n3/so_thinking_ahead_how_do_we_stop_bots) What about the bots that are useful?
- [Why exactly is my comment box at the bottom, rather than the top?](https://tildes.net/~tildes/ov/we_gotta_move_the_comment_box_from_the_bottom_of_the_comments_to_the_top) We have reasons. ;)
- [Anonymous posting?](https://tildes.net/~tildes/qc/discussing_anonymity_on) You betcha. Privacy is not just a buzzword.
- [Can we think of a better name than votes? Not really, not yet.](https://tildes.net/~tildes/b1/suggestion_find_a_new_name_for_vote_button) Got any ideas?
- [Funding - how do we pay for all of this?](https://tildes.net/~tildes/je/lets_talk_about_that_annoying_thing_we_all_dont_want_to_think_about_funding) Nothing is free, after all. No ads, no pay to play, but what else could we do?
- [Tildes Gold?](https://tildes.net/~tildes/ix/tildes_monocle_aka_sticker_aka_gold) No, something much better - the exemplary upvote, because you need a limited use vote to highlight the things you think are top quality. If we all use them together, it just might work.
- [What's up with the tagging system?](https://tildes.net/~tildes/r9/state_of_tags_as_a_mechanic) We've got big plans.
- [Can we edit titles?](https://tildes.net/~tildes/s4/feature_request_title_editing) Good question, we're not sure exactly how it'll work but we want to enable this feature!
- [Can I search Tildes for a post or tag?](https://tildes.net/~tildes/qo/is_there_ability_to_search_for_existing_post_either_by_link_or_by_title_if_not_is_it_planned) Not yet, but it's coming.
- [Moving the vote count to the vote button.](https://tildes.net/~tildes/13w/the_vote_count_for_comments_has_been_moved_to_the_vote_button) It's the little things.

---
Adapted from [this post](https://tildes.net/~tildes/r5/were_starting_to_see_a_lot_of_repeat_questions_so_let_me_make_an_introduction_to_tildes_post_for) from [@Amarok](https://tildes.net/user/Amarok)