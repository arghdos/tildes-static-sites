Title: Reporting Bugs
Slug: bugreporting
Modified: May 28, 2018
Summary: How to report a bug

[TOC]

## Found a Bug?

First, check the [~tildes](https://tildes.net/~tildes/) group and the [issue tracker](https://gitlab.com/tildes/tildes/issues) to see if it's been reported before!
If not, please file a bug in the tracker.